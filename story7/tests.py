from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homepage


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse("story7:homepage")
        

    def test_homepage(self):
        response = self.client.get(self.homepage_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/homepage.html")
        self.assertContains(response, "Aktivitas saat ini")
        self.assertContains(response, "Pengalaman organisasi/kepanitiaan")
        self.assertContains(response, "Prestasi")
        response_function = resolve(self.homepage_url)
        self.assertEqual(response_function.func, homepage)