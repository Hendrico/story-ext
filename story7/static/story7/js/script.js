$(document).ready(function(){

    $("#accordion").accordion();

    $(".up").click((e) => {
        var h3 = e.target.parentElement        
        change(h3,"up")
    })

    $(".down").click((e) => {
        var h3 = e.target.parentElement
        change(h3,"down")
    })

    function change(h3, operation) {
        var accordionList = $("#accordion").children()
        var ul = $("#accordion")

        var pos = accordionList.index(h3)
        var div = accordionList[pos+1]

        console.log(div)
        console.log( accordionList[pos+2])
        if (pos == 0 && operation == "up") return
        if (pos == accordionList.length-1 && operation == "down") return

        if (operation == "up") {
            var h3Up = $(accordionList[pos-2])
            var divUp = $(accordionList[pos-1])

            // Posisi yang dipilih
            $(h3).attr("position", h3Up.attr("position")) 
            $(div).attr("position", divUp.attr("position"))

            // Posisi yg dipengaruhi
            h3Up.attr("position", h3Up.attr("position") + 1) 
            divUp.attr("position", divUp.attr("position") + 1)
        
        } else if (operation == "down") {
            var h3Down = $(accordionList[pos+2])
            var divDown = $(accordionList[pos+3])

            // Posisi yang dipilih
            $(h3).attr("position", h3Down.attr("position")) 
            $(div).attr("position", divDown.attr("position"))

            // Posisi yagng dipengaruhi
            h3Down.attr("position", h3Down.attr("position") - 1) 
            divDown.attr("position", divDown.attr("position") - 1)
        }

        accordionList.sort(sortByPosition)
       
        $.each(accordionList, (index, li) => {
            ul.append(li);
        })
    }

    function sortByPosition(a, b) {
        var item1pos = Number($(a).attr("position"))
        var item2pos = Number($(b).attr("position"))
        
        if (item1pos < item2pos) return -1;
        if (item2pos > item2pos) return 1;
        return 0;
    }

    

    
})
