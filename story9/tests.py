from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homepage, signup
from django.contrib.auth.models import User


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse("story9:homepage")
        self.signup_url = reverse("story9:signup")
        self.login_url = "/story9/accounts/login/?next=/story9"
        self.logout_url = "/story9/accounts/logout/?next=/story9"
        self.test_user = User.objects.create_user("test_username", password="testgoodpassword")
        self.test_user.save()

        

    def test_homepage(self):
        response = self.client.get(self.homepage_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Story 9")
        self.assertContains(response, "Login")
        self.assertContains(response, "Signup")
        self.assertContains(response, "Silahkan login terlebih dahulu untuk melihat konten")
        response_function = resolve(self.homepage_url)
        self.assertEqual(response_function.func, homepage)

    
    def test_login_page(self):
        response = self.client.get(self.login_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Belum punya akun ? Daftar disini")
        
    def test_signup_page(self):
        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "Sign up")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        response_function = resolve(self.signup_url)
        self.assertEqual(response_function.func, signup)  

    def test_signup_form_dan_menampilkan_nama_di_homepage(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "www.tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Selamat Datang nama")
        self.assertContains(response, "Halo, tes")


    
    def test_signup_form_password_berbeda(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagas"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "The two password fields didn’t match")

    def test_signup_form_email_tidak_valid(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "a@f",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "Enter a valid email address")

    def test_signup_form_username_tidak_valid(self):
        response = self.client.post(self.signup_url , {
            'username': "username tidak valid",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters")

    def test_signup_form_password_mirip_username(self):
        response = self.client.post(self.signup_url , {
            'username': "qwerty",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "qwerty",
            'password2': "qwerty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "The password is too similar to the username")

    def test_signup_form_password_terlalu_umum(self):
        response = self.client.post(self.signup_url , {
            'username': "test",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "qwerty",
            'password2': "qwerty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/signup_views.html")
        self.assertContains(response, "This password is too common")

    def test_login_password_salah_atau_tidak_ada_username_terdaftar(self):
        response = self.client.post(self.login_url , {
            'username': "test",
            'password1': "qwerty",
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
        self.assertContains(response, "Username tidak tersedia atau password salah")
        self.assertContains(response, "Silahkan mendaftar jika belum memiliki akun")

    def test_login_berhasil(self):
        response = self.client.post(self.login_url, {
            "username" : "test_username",
            "password" : "testgoodpassword"
        }, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Selamat Datang")
        self.assertContains(response, "Halo, test_username")

    def test_signup_logout_login(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "www.tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Selamat Datang nama")
        self.assertContains(response, "Halo, tes")

        response = self.client.post(self.logout_url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Login")
        self.assertContains(response, "Signup")
        self.assertContains(response, "Silahkan login terlebih dahulu untuk melihat konten")

        response = self.client.post(self.login_url, {
            "username" : "tes",
            "password" : "passwordbagus"
        }, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/homepage.html")
        self.assertContains(response, "Selamat Datang nama")
        self.assertContains(response, "Halo, tes")







        



