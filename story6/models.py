from django.db import models
from django.shortcuts import reverse


class Mahasiswa(models.Model):
    nama = models.TextField(max_length=100, unique=True)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        link = self.nama.replace(" ","-")
        return reverse("story6:detailMahasiswa",args=[link])

class Kegiatan(models.Model):
    nama = models.TextField(max_length=100,unique=True)
    mahasiswa = models.ManyToManyField(Mahasiswa,blank=True)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        link = self.nama.replace(" ","-")
        return reverse("story6:detailKegiatan",args=[link])