from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('addKegiatan/', views.addKegiatan, name='addKegiatan'),
    path("addMahasiswa/<int:id>",views.addMahasiswa,name="addMahasiswa"),
    path("mahasiswa/<str:nama>",views.detailMahasiswa,name="detailMahasiswa"),
    path("kegiatan/<str:nama>",views.detailKegiatan,name="detailKegiatan"),

]
