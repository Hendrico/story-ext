from django import forms
from .models import Kegiatan, Mahasiswa

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        exclude = ["mahasiswa"]

class MahasiswaForm(forms.ModelForm):
    class Meta:
        model = Mahasiswa
        fields = "__all__"
