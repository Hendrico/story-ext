from django.test import TestCase, Client
from django.urls import reverse
from .models import Kegiatan, Mahasiswa

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.mahasiswa_test = Mahasiswa.objects.create(nama="MahasiswaA")
        self.mahasiswaNoKegiatan_test = Mahasiswa.objects.create(nama="MahasiswaZ")
        self.kegiatan_test = Kegiatan.objects.create(nama="KegiatanA")
        self.kegiatan_test.mahasiswa.add(self.mahasiswa_test)
        self.index_url = reverse("story6:homepage")
        self.addKegiatan_url = reverse("story6:addKegiatan")
        self.detailKegiatan_url = reverse("story6:detailKegiatan", args=[self.kegiatan_test.nama])
        self.detailMahasiswa_url = reverse("story6:detailMahasiswa", args=[self.mahasiswa_test.nama])
        self.addMahasiswa_url = reverse("story6:addMahasiswa",args=[self.kegiatan_test.id])

    def test_index_page(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Tambah Kegiatan")
        self.assertContains(response, "Daftar Kegiatan")
        self.assertContains(response, "MahasiswaA")
        self.assertContains(response, "KegiatanA")

    def test_add_kegiatan_page(self):
        response = self.client.get(self.addKegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "addKegiatan.html")
        self.assertContains(response, "Daftar Kegiatan Baru")
        self.assertContains(response, "Nama Kegiatan")
        self.assertContains(response, "Tambah Kegiatan")
        self.assertContains(response, "Confirm")

    def test_detail_kegiatan(self):
        response = self.client.get(self.detailKegiatan_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("detailKegiatan.html")
        self.assertContains(response, "KegiatanA")
        self.assertContains(response, "MahasiswaA")

    def test_detail_mahasiswa(self):
        response = self.client.get(self.detailMahasiswa_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("detailKegiatan.html")
        self.assertContains(response, "KegiatanA")
        self.assertContains(response, "MahasiswaA")

    def test_add_kegiatan_lalu_redirect(self):
        response = self.client.post(self.addKegiatan_url, {
            "nama" : "KegiatanB"
        })
        self.assertEquals(response.status_code,302)
    
    def test_add_mahasiswa_baru(self):
        response = self.client.post(self.addMahasiswa_url, {
            "nama" : "MahasiswaB"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Mahasiswa.objects.all().count(), 3)

    def test_add_mahasiswa_yang_sudah_di_DB(self):
        response = self.client.post(self.addMahasiswa_url, {
            "nama" : "MahasiswaZ"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Mahasiswa.objects.all().count(), 2)
    
    def test_add_mahasiswa_yang_sudah_di_kegiatan(self):
        response = self.client.post(self.addMahasiswa_url, {
            "nama" : "MahasiswaB"
        })
        response = self.client.post(self.addMahasiswa_url, {
            "nama" : "MahasiswaB"
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Mahasiswa.objects.all().count(), 3)


class TestModels(TestCase):
    def setUp(self):
        self.mahasiswa_test = Mahasiswa.objects.create(nama="MahasiswaA")
        self.kegiatan_test = Kegiatan.objects.create(nama="KegiatanA")

    def test_model_mahasiswa(self):
        total = Mahasiswa.objects.all().count()
        self.assertEquals(total, 1)
        
    def test_model_kegiatan(self):
        total = Kegiatan.objects.all().count()
        self.assertEquals(total, 1)

    def test_nama_mahasiswa(self):
        self.assertEquals(str(self.mahasiswa_test),"MahasiswaA")

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test),"KegiatanA")

