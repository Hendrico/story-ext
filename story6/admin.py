from django.contrib import admin
from .models import Mahasiswa, Kegiatan


admin.site.register(Mahasiswa)
admin.site.register(Kegiatan)

