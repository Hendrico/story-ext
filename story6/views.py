from django.shortcuts import get_object_or_404, redirect, render, reverse
from .models import Mahasiswa, Kegiatan
from .forms import MahasiswaForm, KegiatanForm


def homepage(request):
    context = {
        "kegiatanList" : Kegiatan.objects.all(),
    }
    return render(request, 'homepage.html', context)

def addKegiatan(request):
    if (request.method == "POST"):
        kegiatanForm = KegiatanForm(request.POST)
        if (kegiatanForm.is_valid()):
            kegiatanForm.save()
        return redirect("story6:homepage")
    return render(request,"addKegiatan.html")

def detailKegiatan(request, nama):
    nama = nama.replace("-"," ")
    kegiatan = get_object_or_404(Kegiatan,nama=nama)
    context = {
        "kegiatan" : kegiatan,
    }
    return render(request,"detailKegiatan.html",context)


def addMahasiswa(request, id):
    if (request.method == "POST"):
        kegiatan = Kegiatan.objects.get(id=id)
        namaMahasiswa = request.POST["nama"]

        mahasiswaBaru, isCreate = Mahasiswa.objects.get_or_create(nama=namaMahasiswa)
        if (isCreate):
            mahasiswaBaru.save()
            kegiatan.mahasiswa.add(mahasiswaBaru) # Membuat dan menambah objek mahasiswa baru
        else:
            try:
                kegiatan.mahasiswa.get(nama=namaMahasiswa) # Jangan add peserta yang sudah di kegiatan.
            except:
                kegiatan.mahasiswa.add(mahasiswaBaru) # Add peserta baru yang ada di database tapi tidak di kegiatan.
    return redirect(reverse("story6:detailKegiatan",args=[kegiatan.nama.replace(" ","-")]))

def detailMahasiswa(request,nama):
    nama = nama.replace("-"," ")
    mahasiswa = get_object_or_404(Mahasiswa,nama=nama)
    context = {
        "mahasiswa" : mahasiswa
    }
    return render(request,"detailMahasiswa.html",context)
