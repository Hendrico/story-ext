from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


def homepage(request):
    return render(request, 'story8/homepage.html')

def search(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    jsonData = json.loads(requests.get(url).content)
    return JsonResponse(jsonData, safe=False)


