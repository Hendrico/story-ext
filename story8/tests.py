from django.test import TestCase
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homepage


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse("story8:homepage")
        

    def test_homepage(self):
        response = self.client.get(self.homepage_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story8/homepage.html")
        self.assertContains(response, "Books Searcher")
        response_function = resolve(self.homepage_url)
        self.assertEqual(response_function.func, homepage)

    
    def test_cari_buku(self):
        response = self.client.get(reverse('story8:search') + '?q=red')
        self.assertEqual(response.status_code, 200)


