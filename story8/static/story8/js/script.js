var nullImage = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.m.wikipedia.org%2Fwiki%2FFile%3ANo_image_available.svg&psig=AOvVaw1_egfVX9wdjIWja6cao-FC&ust=1607234775074000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPD09qCWtu0CFQAAAAAdAAAAABAD"

$(document).ready(function(){

    $("#search").keyup(function(){
        var input = $("#search").val();
        console.log(input);

        $.ajax({
            url: '/story8/search?q=' + input,
            success: function(data){
                var items = data.items;
                console.log(items);
                $("#items-list").empty();
                for (i = 0; i < items.length; i++){
                    var judul = items[i].volumeInfo.title;
                    var html = "<li class=\"items-list\">" + judul + "<br>"; 
                    
                    try {
                        var author = items[i].volumeInfo.authors[0];
                        html += "Author : " + author + "<br>";
                    }
                    catch(err) {
                        html += "No author" + "<br>";
                    }

                    try {
                        var gambar = items[i].volumeInfo.imageLinks.smallThumbnail;
                        html += "<img src=" + gambar + "<br>";
                        
                    }
                    catch(err) {
                        html += "<img src=" + nullImage + "><br>";
                    }
                    
                    html += "</li>"
                    $("#items-list").append(html);
                }
            }
        })
    })
    
})
