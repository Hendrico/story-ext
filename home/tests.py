from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homepage


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.home_url = reverse("home:homepage")
        

    def test_homepage(self):
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "home.html")
        self.assertContains(response, "List Story")
        response_function = resolve(self.home_url)
        self.assertEqual(response_function.func, homepage)

        
